module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);
  grunt.loadNpmTasks('grunt-execute');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.initConfig({
    clean: ["dist"],

    babel: {
      options: {
        sourceMap: false,
        modules: "common"
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'src',
          src: [
            '**/*.js',
          ],
          dest: 'dist/src',
        }]
      }
    },

    mochaTest: {
      test: {
        options: {
          reporter: 'spec'
        },
        src: ['dist/src/**/*.spec.js']
      }
    }

  });

  grunt.registerTask('default', ['clean', 'babel', 'mochaTest']);
  grunt.registerTask('build', ['clean', 'babel']);
};