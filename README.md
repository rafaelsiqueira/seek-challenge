### The problem ###

Apply a special discount based on customer's price rule

### Solution design ###

I've designed the solution using the Strategy design pattern. This a good choice when you need to choose an algorithm in runtime.

### Project Structure ###

The project was separated in two parts such as *main source* and *tests*. It was organized in packages as described below:

- main
    - model - all database (mocked) entities and rules
    - strategy - all the strategies to handle/apply price rules are grouped here
- test
    - fixtures - all data to support the tests
    - model - all tests related with main/model package
    - strategy - all tests related with main/strategy package

### Dependencies (build and tests) ###

- babel
- grunt
- lodash
- chai
- mocha

### How to run? ###

`npm start`

### How to test? ###

`npm test`