import * as Fixtures from './test/fixtures';

Object.keys(Fixtures).forEach((cartKey) => {
  const message = `${cartKey} total: $${Fixtures[cartKey].total()}`;
  console.log(message);
  console.log('='.repeat(message.length));
});