import StrategyFactory from '../strategy/factory';

export default class Cart {

  /**
   *
   * @param {DefaultCustomer} customer
   */
  constructor (customer) {
    this.items = [];
    this.customer = customer;
  }

  /**
   *
   * @param {{}} product
   */
  add (product) {
    this.items.push({...product});
  }

  /**
   * Calculates total amount applying discount strategies
   * @returns {Number}
   */
  total () {

    if (!this.customer) {
      throw Error('Customer not set');
    }

    // if cart is empty
    if (!this.items || this.items.length === 0) {
      return 0;
    }

    // if there's no price rule configured for the customer
    if (!this.customer.priceRule) {
      return StrategyFactory
        .create({type: 'default'}, this.items)
        .execute();
    }

    this.customer.validatePriceRule();

    // execute the strategy for each price rule
    return this.customer.priceRule
      .map((rule) => {
        return StrategyFactory
          .create(rule, this.items)
          .execute()
      })
      .reduce((a,b) => a+b, 0);
  }
}