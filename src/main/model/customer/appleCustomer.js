import { ClassicProduct, StandoutProduct, PremiumProduct } from '../products';
import DefaultCustomer from './defaultCustomer';

export default class AppleCustomer extends DefaultCustomer {

  static get priceRule () {
    return [
      {
        type: 'default',
        product: ClassicProduct.id,
      },
      {
        type: 'priceDrop',
        product: StandoutProduct.id,
        priceDrop: 299.99,
        minimal: 1
      },
      {
        type: 'default',
        product: PremiumProduct.id
      }
    ]
  }
};