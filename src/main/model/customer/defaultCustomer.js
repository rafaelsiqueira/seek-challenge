import _ from 'lodash';

export default class {

  static get priceRule() {
    return null;
  }

  /**
   * Validates price rule settings
   */
  static validatePriceRule() {
    const rules = _.groupBy(this.priceRule, 'product');
    Object.keys(rules).map((product) => {
      if (rules[product].length > 1) {
        throw Error('Error: Multiple rules for the same product is not allowed.');
      }
    });
  }
}