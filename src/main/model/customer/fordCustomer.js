import { ClassicProduct, StandoutProduct, PremiumProduct } from '../products';
import DefaultCustomer from './defaultCustomer';

export default class FordCustomer extends DefaultCustomer {

  static get priceRule () {
    return [
      {
        type: 'buyXEarnY',
        product: ClassicProduct.id,
        buy: 5,
        earn: 1
      },
      {
        type: 'priceDrop',
        product: StandoutProduct.id,
        priceDrop: 309.99,
        minimal: 1
      },
      {
        type: 'priceDrop',
        product: PremiumProduct.id,
        priceDrop: 389.99,
        minimal: 3
      }
    ]
  }
};