import DefaultCustomer from './defaultCustomer';
import UnileverCustomer from './unileverCustomer';
import AppleCustomer from './appleCustomer';
import NikeCustomer from './nikeCustomer';
import FordCustomer from './fordCustomer';

export {
  DefaultCustomer,
  UnileverCustomer,
  AppleCustomer,
  NikeCustomer,
  FordCustomer
};