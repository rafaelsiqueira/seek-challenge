import { ClassicProduct, StandoutProduct, PremiumProduct } from '../products';
import DefaultCustomer from './defaultCustomer';

export default class NikeCustomer extends DefaultCustomer {

  static get priceRule () {
    return [
      {
        type: 'default',
        product: ClassicProduct.id
      },
      {
        type: 'default',
        product: StandoutProduct.id,
      },
      {
        type: 'priceDrop',
        product: PremiumProduct.id,
        priceDrop: 379.99,
        minimal: 4
      }
    ]
  }

};