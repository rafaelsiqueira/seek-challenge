import { ClassicProduct, StandoutProduct, PremiumProduct } from '../products';
import DefaultCustomer from './defaultCustomer';

export default class UnileverCustomer extends DefaultCustomer {

  static get priceRule () {
    return [
      {
        type: 'buyXEarnY',
        product: ClassicProduct.id,
        buy: 3,
        earn: 1
      },
      {
        type: 'default',
        product: StandoutProduct.id,
      },
      {
        type: 'default',
        product: PremiumProduct.id
      }
    ]
  }

};