// this is a mock representing the database

const ClassicProduct = {
  id: 'classic',
  name: 'Classic Ad',
  price: 269.99
};

const StandoutProduct = {
  id: 'standout',
  name: 'Standout Ad',
  price: 322.99
};

const PremiumProduct = {
  id: 'premium',
  name: 'Premium Ad',
  price: 394.99
};

export { ClassicProduct, StandoutProduct, PremiumProduct };