import DefaultStrategy from './defaultStrategy';
import _ from 'lodash';

export default class BuyXEarnYStrategy extends DefaultStrategy {

  execute () {
    // filters only rule's product
    const filteredItems = this._filterByProductRule();

    // splits available items by rule's "buy" setting and filter using the same value.
    // the result will be the chuncks matched with the rule
    const chunks = _.chunk(filteredItems, this.rule.buy)
      .map(this.applyEarns.bind(this));

    return _.sum(
      _.flatten(chunks)
      .map((item) => item.price)
    );
  }

  applyEarns (chunk) {
    // for each chunk, check if the length matches the price rule
    if (chunk.length === this.rule.buy) {
      // if so, use the "earn" setting to slice the chunk's items
      const itemsToApplyDiscount = [...chunk].splice(0, this.rule.earn);

      // iterates the result and set items value to zero
      itemsToApplyDiscount.map((item) => {
        item.price = 0.00; return item;
      });
    }
    return chunk;
  }
}