import _ from 'lodash';

export default class DefaultStrategy {

  constructor (rule, items) {
    this.rule = rule;
    this.items = items;
  }

  execute () {
    let items = this.items;

    if (this.rule.product) {
      items = this._filterByProductRule();
    }

    return _.sum(items
      .map((i) => i.price)
    );
  }

  _filterByProductRule() {
    return this.items.filter((item) => item.id === this.rule.product);
  }
}