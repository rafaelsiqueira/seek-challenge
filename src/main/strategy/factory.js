import DefaultStrategy from './defaultStrategy';
import PriceDropStrategy from './priceDropStrategy';
import BuyXEarnYStrategy from './buyXEarnYStrategy';

export default class StrategyFactory {
  static create(rule, items) {
    let strategy;

    if (!rule || !rule.type) {
      throw Error('Invalid strategy!');
    }

    switch (rule.type) {
      case 'priceDrop':
        strategy = new PriceDropStrategy(rule, items);
        break;

      case 'buyXEarnY':
        strategy = new BuyXEarnYStrategy(rule, items);
        break;

      default:
        strategy = new DefaultStrategy(rule, items);
        break;
    }

    return strategy;
  }
}