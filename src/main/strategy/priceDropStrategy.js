import DefaultStrategy from './defaultStrategy';
import _ from 'lodash';

export default class PriceDropStrategy extends DefaultStrategy {

  execute () {
    // filters only products known by the strategy
    const filteredItems = this._filterByProductRule();

    // if the total items matches the minimal setting
    if (filteredItems.length >= this.rule.minimal) {

      // apply the discount price for each item
      filteredItems.map((item) => item.price = this.rule.priceDrop);
    }

    return _.sum(filteredItems
      .map((item) => item.price)
    );
  }
}