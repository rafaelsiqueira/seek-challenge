import { ClassicProduct, StandoutProduct, PremiumProduct } from '../../main/model/products';
import Cart from '../../main/model/cart';
import {
  DefaultCustomer, UnileverCustomer, AppleCustomer,
  NikeCustomer, FordCustomer
} from '../../main/model/customer';

const cartDefaultCustomer = new Cart(DefaultCustomer);
cartDefaultCustomer.add(ClassicProduct);
cartDefaultCustomer.add(StandoutProduct);
cartDefaultCustomer.add(PremiumProduct);
export { cartDefaultCustomer };

const cartUnileverCustomer = new Cart(UnileverCustomer);
cartUnileverCustomer.add(ClassicProduct);
cartUnileverCustomer.add(ClassicProduct);
cartUnileverCustomer.add(ClassicProduct);
cartUnileverCustomer.add(PremiumProduct);
export { cartUnileverCustomer };

const cartAppleCustomer = new Cart(AppleCustomer);
cartAppleCustomer.add(StandoutProduct);
cartAppleCustomer.add(StandoutProduct);
cartAppleCustomer.add(StandoutProduct);
cartAppleCustomer.add(PremiumProduct);
export { cartAppleCustomer };

const cartNike = new Cart(NikeCustomer);
cartNike.add(PremiumProduct);
cartNike.add(PremiumProduct);
cartNike.add(PremiumProduct);
cartNike.add(PremiumProduct);
export { cartNike };

const cartFord = new Cart(FordCustomer);
cartFord.add(ClassicProduct);
cartFord.add(ClassicProduct);
cartFord.add(ClassicProduct);
cartFord.add(ClassicProduct);
cartFord.add(ClassicProduct);
cartFord.add(StandoutProduct);
cartFord.add(PremiumProduct);
cartFord.add(PremiumProduct);
cartFord.add(PremiumProduct);
export { cartFord };