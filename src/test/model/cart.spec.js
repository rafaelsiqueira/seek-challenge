import { expect } from 'chai';
import Assert from 'assert';
import Cart from '../../main/model/cart';
import DefaultCustomer from '../../main/model/customer/defaultCustomer';
import * as Fixtures from '../fixtures';

describe('Cart', () => {

  describe('#add()', () => {
    it('should add elements in cart.items array', () => {
      const cart = new Cart();
      Assert.equal(cart.items.length, 0);
      cart.add({product: 'test'});
      Assert.equal(cart.items.length, 1);
    });
  });

  describe('#total()', () => {
    it('should throw error when customer is not set', () => {
      const cart = new Cart();
      expect(cart.total).to.throw(Error);
    });

    it('should returns 0 if cart is empty', () => {
      const cart = new Cart(DefaultCustomer);
      expect(cart.total).to.be.zero;
    });

    it('should default customer total\'s cart be equals 987.97', () => {
      expect(Fixtures.cartDefaultCustomer.total()).to.be.equal(987.97);
    });

    it('should unilever customer total\'s cart be equals 934.97', () => {
      expect(Fixtures.cartUnileverCustomer.total()).to.be.equal(934.97);
    });

    it('should apple customer total\'s cart be equals 1294.96', () => {
      expect(Fixtures.cartAppleCustomer.total()).to.be.equal(1294.96);
    });

    it('should nike customer total\'s cart be equals 1519.96', () => {
      expect(Fixtures.cartNike.total()).to.be.equal(1519.96);
    });

    it('should ford customer total\'s cart be equals 2559.92', () => {
      expect(Fixtures.cartFord.total()).to.be.equal(2559.92);
    });

  })
});