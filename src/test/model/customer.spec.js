import { expect } from 'chai';
import DefaultCustomer from '../../main/model/customer/defaultCustomer';
import { ClassicProduct } from '../../main/model/products';

describe('Customer', () => {

  describe('#validatePriceRule()', () => {
    it('should throw Error if customer has more than one price rule for the same product', () => {

      const defaultCustomer = Object.assign({}, DefaultCustomer, {
        priceRule() {
          return [
            {
              type: 'default',
              product: ClassicProduct.id,
            },
            {
              type: 'priceDrop',
              product: ClassicProduct.id,
              priceDrop: 1.00,
              minimal: 1
            }
          ]
        }
      });

      expect(defaultCustomer.priceRule().length).to.be.one;
      expect(DefaultCustomer.validatePriceRule).to.throw(Error);
    });
  });
});