import { expect } from 'chai';
import BuyXEarnYStrategy from '../../main/strategy/buyXEarnYStrategy'
import { ClassicProduct, StandoutProduct } from '../../main/model/products';

describe('BuyXEarnYStrategy', () => {
  describe('#execute()', () => {

    it('should gives one ClassicProduct for free when buying 3 ClassicProducts', () => {
      const rule = {
        product: ClassicProduct.id,
        buy: 3,
        earn: 1
      };

      const strategy = new BuyXEarnYStrategy(rule, [
        {...ClassicProduct}, {...ClassicProduct}, {...ClassicProduct}
      ]);

      const expectedPrice = ClassicProduct.price * (rule.buy - rule.earn);
      expect(strategy.execute()).to.be.equal(expectedPrice);
    });

    it('should gives two StandoutProduct for free when buying 6 StandoutProduct', () => {
      const rule = {
        product: StandoutProduct.id,
        buy: 6,
        earn: 2
      };

      const strategy = new BuyXEarnYStrategy(rule, [
        {...StandoutProduct}, {...StandoutProduct}, {...StandoutProduct},
        {...StandoutProduct}, {...StandoutProduct}, {...StandoutProduct}
      ]);

      const expectedPrice = StandoutProduct.price * (rule.buy - rule.earn);
      expect(strategy.execute()).to.be.equal(expectedPrice);
    });

  });
});