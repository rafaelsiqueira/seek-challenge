import { expect } from 'chai';
import DefaultStrategy from '../../main/strategy/defaultStrategy'
import { ClassicProduct } from '../../main/model/products';

describe('DefaultStrategy', () => {
  describe('#execute()', () => {

    it('should return original price from items', () => {

      const strategy = new DefaultStrategy({}, [
        {...ClassicProduct}, {...ClassicProduct}, {...ClassicProduct}
      ]);

      const expectedPrice = ClassicProduct.price * strategy.items.length;
      expect(strategy.execute()).to.be.equal(expectedPrice);
    });
  });
});