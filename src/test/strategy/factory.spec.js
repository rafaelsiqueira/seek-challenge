import { expect } from 'chai';
import StrategyFactory from '../../main/strategy/factory';
import DefaultStrategy from '../../main/strategy/defaultStrategy';
import BuyXEarnYStrategy from '../../main/strategy/buyXEarnYStrategy';
import PriceDropStrategy from '../../main/strategy/priceDropStrategy';

describe('StrategyFactory', () => {
  describe('#create()', () => {

    it('should throw Error when create rule null', () => {
      expect(StrategyFactory.create).to.throw(Error);
    });

    it('should create DefaultStrategy', () => {
      expect(StrategyFactory.create({type: 'default'})).to.instanceof(DefaultStrategy);
    });

    it('should create BuyXEarnYStrategy', () => {
      expect(StrategyFactory.create({type: 'buyXEarnY'})).to.instanceof(BuyXEarnYStrategy);
    });

    it('should create PriceDropStrategy', () => {
      expect(StrategyFactory.create({type: 'priceDrop'})).to.instanceof(PriceDropStrategy);
    });
  });
});

