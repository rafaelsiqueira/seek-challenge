import { expect } from 'chai';
import PriceDropStrategy from '../../main/strategy/priceDropStrategy'
import { ClassicProduct } from '../../main/model/products';

describe('PriceDropStrategy', () => {
  describe('#execute()', () => {

    it('should drop price of ClassicProduct to 1.00 when buying 3', () => {
      const rule = {
        product: ClassicProduct.id,
        minimal: 3,
        priceDrop: 1.00
      };

      const strategy = new PriceDropStrategy(rule, [
        {...ClassicProduct}, {...ClassicProduct}, {...ClassicProduct}
      ]);

      const expectedPrice = rule.priceDrop * strategy._filterByProductRule().length;
      expect(strategy.execute()).to.be.equal(expectedPrice);
    });
  });
});